/**
 * All application routes.
 */
import { examplesController } from './controllers/examples.controller';

export const AppRoutes = [
    {
        path: '/example/hi',
        method: 'post',
        action: examplesController
    }
];
