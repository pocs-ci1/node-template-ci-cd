import { Request, Response } from 'express';
import { ExamplesService } from '../services/examples.service';

export async function examplesController(request: Request, response: Response) {
    try {
        let service = new ExamplesService();
        const validationOk = await service.sendHi(request, response);
        return response.send(validationOk);
    } catch (e) {
        throw e;
    }
}