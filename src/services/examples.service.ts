import { Request, Response } from 'express';
import { ExampleDto } from '../dtos/example.dto';

export class ExamplesService {

    constructor() {
    }

    public async sendHi(request: Request, response: Response): Promise<string> {
        const valid = new ExampleDto(request.body.subject, request.body.to, request.body.nameTo,
            request.body.text,
            request.body.tmpl, request.body.firma, request.body.ente);
        await valid.validate();
        return 'Hi, validation ok.'
    }
}