import { ValidationError } from 'class-validator';

export class ValidationDtoError implements Error {
    name: string;
    message: string;
    errors: ({ [p: string]: string } | undefined)[];
    stack?: string | undefined;

    constructor(error: ValidationError[]) {

        this.name = 'VALIDATION_ERROR';
        this.errors = error.map(e => e.constraints);
        this.message = ``;
    }
}