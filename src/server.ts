import express, { Request, Response } from 'express';
import 'dotenv/config';
import * as bodyParser from 'body-parser';
import { AppRoutes } from './routes';
import helmet from 'helmet';

const errorHandler = require('./middlewares/error-handler.mid');

const app: express.Application = express();
app.use(helmet())
app.use(bodyParser.json());

AppRoutes.forEach((route) => {
    app.use(
        route.path,
        (request: Request, response: Response, next: Function) => {
            route
                .action(request, response)
                .then((re) => next)
                .catch((err) => {
                    console.log(err);
                    return next(err);
                });
        }
    );
});

app.use(errorHandler);

app.listen(process.env.PORT, function () {
    console.log(`Server running on http://127.0.0.1: ${process.env.PORT}!`);
});
