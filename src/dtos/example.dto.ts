import { IsNotEmpty, IsOptional, Matches, MaxLength, validateOrReject } from 'class-validator';
import { ValidationDtoError } from '../errors/validation-dto.error';

export const EMAIL_REGEXP = /^(?=.{1,254}$)(?=.{1,64}@)[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+(\.[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+)*@[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?(\.[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?)*$/;

export class ExampleDto {
    @MaxLength(200)
    @IsNotEmpty()
    public subject: string;

    @MaxLength(100)
    @Matches(EMAIL_REGEXP)
    @IsNotEmpty()
    public to: string;

    @MaxLength(200)
    @IsNotEmpty()
    public nameTo: string;

    @MaxLength(3000)
    @IsNotEmpty()
    public text: string;

    @MaxLength(200)
    @IsNotEmpty()
    public firma: string;

    @MaxLength(200)
    @IsOptional()
    public ente: string | undefined;

    constructor(subject: string, to: string, nameTo: string, text: string, tmpl: string, firma1: string,
                firma2?: string) {
        this.subject = subject;
        this.nameTo = nameTo;
        this.to = to;
        this.text = text;
        this.firma = firma1;
        this.ente = firma2;
    }

    public async validate() {
        try {
            await validateOrReject(this, {validationError: {target: false}});
        } catch (e) {
            throw new ValidationDtoError(e);
        }
    }
}
