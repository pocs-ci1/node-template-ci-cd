FROM node:14-alpine AS builder
# Create root application folder
WORKDIR /mail
COPY .env ./
COPY *.json ./
COPY src ./src
# Install all Packages
RUN npm install
RUN npm run build:prod


FROM node:14-alpine
RUN npm install pm2 -g --silent
# Set the timezone in docker
ENV TIME_ZONE=America/Argentina/Cordoba
ENV ENVIROMENT=dev
RUN apk --update add tzdata \
   && cp /usr/share/zoneinfo/America/Argentina/Cordoba /etc/localtime \
   && echo "America/Argentina/Cordoba" > /etc/timezone \
   && apk del tzdata
   
WORKDIR /mail
COPY --from=builder /mail ./
EXPOSE 3003
CMD ["sh", "-c", "pm2-runtime ./dist/server-obfuscated.js"]
